﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaiOnCorpProj.Models
{
    public class CPromotionAndFProductViewModel
    {
        public IEnumerable<CurrentPromotion> CurrentPromotion { get; set; }
        public IEnumerable<FeaturedProduct> FeaturedProduct { get; set; }
    }
}
