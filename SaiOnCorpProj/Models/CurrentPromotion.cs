﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaiOnCorpProj.Models
{
    public class CurrentPromotion
    {
        public Int64 Id { get; set; }
        public string Header { get; set; }
        public double price { get; set; }
        public string Description { get; set; }
        public string ViewMore { get; set; }
    }
}
