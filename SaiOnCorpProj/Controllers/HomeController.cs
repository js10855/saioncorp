﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SaiOnCorpProj.Models;
using SaiOnCorpProj.DAL;

namespace SaiOnCorpProj.Controllers
{
    public class HomeController : Controller
    {
        private ICurrentPromotionRepository currentPromotionRepository;
        private IFeaturedProductRepository featuredProductRepository;

        public HomeController(ICurrentPromotionRepository currentPromotionRepository, IFeaturedProductRepository featuredProductRepository)
        {
            this.currentPromotionRepository = currentPromotionRepository;
            this.featuredProductRepository = featuredProductRepository;
        }

        public IActionResult Index()
        {
            //return View();
            var model = new CPromotionAndFProductViewModel();
            model.CurrentPromotion = currentPromotionRepository.GetAllCurrentPromotions();
            model.FeaturedProduct = featuredProductRepository.GetAllFeaturedProducts(); 
            return View(model);
        }

        //public IActionResult About()
        //{
        //    ViewData["Message"] = "Your application description page.";

        //    return View();
        //}

        //public IActionResult Contact()
        //{
        //    ViewData["Message"] = "Your contact page.";

        //    return View();
        //}

        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
