﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SaiOnCorpProj.Models;

namespace SaiOnCorpProj.DAL
{
    public class FeaturedProductMap
    {
        public FeaturedProductMap(EntityTypeBuilder<FeaturedProduct> entityBuilder)
        {
            entityBuilder.HasKey(t => t.Id);
            entityBuilder.Property(t => t.Header).IsRequired();
            entityBuilder.Property(t => t.price).IsRequired();
            entityBuilder.Property(t => t.Description).IsRequired();
            entityBuilder.Property(t => t.ViewMore).IsRequired();
        }
    }
}
