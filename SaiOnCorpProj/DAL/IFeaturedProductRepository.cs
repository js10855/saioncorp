﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SaiOnCorpProj.Models;

namespace SaiOnCorpProj.DAL
{
    public interface IFeaturedProductRepository
    {
        void SaveFeaturedProduct(FeaturedProduct FeaturedProduct);
        IEnumerable<FeaturedProduct> GetAllFeaturedProducts();
        FeaturedProduct GetFeaturedProduct(long id);
        void DeleteFeaturedProduct(long id);
        void UpdateFeaturedProduct(FeaturedProduct FeaturedProduct);
    }
}
