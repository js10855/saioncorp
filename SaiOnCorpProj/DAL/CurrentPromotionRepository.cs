﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SaiOnCorpProj.Models;
using Microsoft.EntityFrameworkCore;

namespace SaiOnCorpProj.DAL
{
    public class CurrentPromotionRepository:ICurrentPromotionRepository
    {
        private CurrentPromotionContext context;
        private DbSet<CurrentPromotion> CurrentPromotionEntity;
        public CurrentPromotionRepository(CurrentPromotionContext context)
        {
            this.context = context;
            CurrentPromotionEntity = context.Set<CurrentPromotion>();
        }


        public void SaveCurrentPromotion(CurrentPromotion CurrentPromotion)
        {
            context.Entry(CurrentPromotion).State = EntityState.Added;
            context.SaveChanges();
        }

        public IEnumerable<CurrentPromotion> GetAllCurrentPromotions()
        {
            return CurrentPromotionEntity.AsEnumerable();
        }

        public CurrentPromotion GetCurrentPromotion(long id)
        {
            return CurrentPromotionEntity.SingleOrDefault(s => s.Id == id);
        }
        public void DeleteCurrentPromotion(long id)
        {
            CurrentPromotion CurrentPromotion = GetCurrentPromotion(id);
            CurrentPromotionEntity.Remove(CurrentPromotion);
            context.SaveChanges();
        }
        public void UpdateCurrentPromotion(CurrentPromotion CurrentPromotion)
        {
            context.SaveChanges();
        }
    }
}
