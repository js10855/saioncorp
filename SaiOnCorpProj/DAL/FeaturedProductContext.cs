﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SaiOnCorpProj.Models;

namespace SaiOnCorpProj.DAL
{
    public class FeaturedProductContext:DbContext
    {
        public FeaturedProductContext(DbContextOptions<FeaturedProductContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new FeaturedProductMap(modelBuilder.Entity<FeaturedProduct>());
        }
    }
}
