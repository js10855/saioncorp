﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SaiOnCorpProj.Models;

namespace SaiOnCorpProj.DAL
{
    public interface ICurrentPromotionRepository
    {
        void SaveCurrentPromotion(CurrentPromotion CurrentPromotion);
        IEnumerable<CurrentPromotion> GetAllCurrentPromotions();
        CurrentPromotion GetCurrentPromotion(long id);
        void DeleteCurrentPromotion(long id);
        void UpdateCurrentPromotion(CurrentPromotion CurrentPromotion);
    }
}
