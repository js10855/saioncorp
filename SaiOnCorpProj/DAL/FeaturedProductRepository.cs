﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SaiOnCorpProj.Models;
using Microsoft.EntityFrameworkCore;

namespace SaiOnCorpProj.DAL
{
    public class FeaturedProductRepository:IFeaturedProductRepository
    {
        private FeaturedProductContext context;
        private DbSet<FeaturedProduct> FeaturedProductEntity;
        public FeaturedProductRepository(FeaturedProductContext context)
        {
            this.context = context;
            FeaturedProductEntity = context.Set<FeaturedProduct>();
        }


        public void SaveFeaturedProduct(FeaturedProduct FeaturedProduct)
        {
            context.Entry(FeaturedProduct).State = EntityState.Added;
            context.SaveChanges();
        }

        public IEnumerable<FeaturedProduct> GetAllFeaturedProducts()
        {
            return FeaturedProductEntity.AsEnumerable();
        }

        public FeaturedProduct GetFeaturedProduct(long id)
        {
            return FeaturedProductEntity.SingleOrDefault(s => s.Id == id);
        }
        public void DeleteFeaturedProduct(long id)
        {
            FeaturedProduct FeaturedProduct = GetFeaturedProduct(id);
            FeaturedProductEntity.Remove(FeaturedProduct);
            context.SaveChanges();
        }
        public void UpdateFeaturedProduct(FeaturedProduct FeaturedProduct)
        {
            context.SaveChanges();
        }
    }
}
